//路由配置
import Vue from 'vue'
import Router from 'vue-router'
//导入Test组件
// import Test from '../components/admin/Test'
// import Test01 from '../components/admin/Test01'
// import Login from '../components/admin/Login'
import AdminLogin from '../components/admin/Login'
Vue.use(Router)
import AdminIndex from "../components/admin/AdminIndex";
import Regist from "../components/admin/Regist";
import UserInfo from "../components/admin/UserInfo";

import Modify from "../components/admin/Modify";


import SalaryInfo from "../components/admin/SalaryInfo.vue";
import AttendInfo from "../components/admin/AttendInfo.vue";
import StaffInfo from "../components/admin/StaffInfo";
import MyInfo from "../components/user/MyInfo.vue";
import Analyze from "../components/analyze/Analyze.vue";
import SalaryAnalyze from "../components/analyze/SalaryAnalyze.vue";
import StaffTransfer from "../components/admin/StaffTransfer.vue";

export default new Router({//新建路由router
  mode:"history",//去除:"#/"
  routes: [
    {
      path: '/',
      name: 'AdminLogin',
      component: AdminLogin
    },
    {
      path: '/Regist',
      name: 'Regist',
      component: Regist
    },

    {
      path: '/Regist',
      name: 'Regist',
      component: Regist
    },
    {
      path:'/Index',
      name:'AdminIndex',
      component: AdminIndex,
      meta:{"isLogin":true},//登录后才能访问主页
      children:[
        // {
        //   path:'Test01',
        //   name:'Test01',
        //   component: Test01
        // },
        {
          path:'UserInfo',
          name:'UserInfo',
          component: UserInfo
        },
        {

          path: 'Modify',
          name: 'Modify',
          component: Modify
        },
        {
          path:'SalaryInfo',
          name:'SalaryInfo',
          component: SalaryInfo
        },
        {
          path:'AttendInfo',
          name:'AttendInfo',
          component: AttendInfo
        },
        {
          path:'StaffInfo',
          name:'StaffInfo',
          component: StaffInfo
        },
        {
          path:'MyInfo/:id',
          name:'MyInfo',
          component: MyInfo
        },
        {
          path:'Analyze',
          name:'Analyze',
          component: Analyze
        },
        {
          path:'SalaryAnalyze',
          name:'SalaryAnalyze',
          component: SalaryAnalyze
        },
        {
          path:'StaffTransfer',
          name:'StaffTransfer',
          component: StaffTransfer
        }
      ]
    }
  ]
})
