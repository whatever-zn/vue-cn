//主配置文件
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'//导入库
import App from './App'
import ElementUI from 'element-ui';//引入element组件
import 'element-ui/lib/theme-chalk/index.css'
import router from './router'
Vue.use(ElementUI);//引用Element组件
//引入axios
import axios from "axios"
import qs from 'qs'
Vue.config.productionTip = false
/* 设置代理的域名 */
axios.defaults.baseURL = '/cn';
/* 设置cookie,session跨域配置 */
axios.defaults.withCredentials=true;
/* 设置post请求体 表单提交*/
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
/* 设置全局axios写法 */
Vue.prototype.$http = axios
/* 设置全局qs写法 */
Vue.prototype.$qs = qs
//定义导航钩子
router.beforeEach (function (to,from,next) {
//to 下一个路由对象， from 上一个路由对象 ,next 函数配置登录的地址
  //将存入绘画存储的用户名用作标识
  const name = sessionStorage.getItem("name");
  console.log("name="+name);
  console.log("to.meta.isLogin ="+to.meta.isLogin);
  //判断哪些链接需要拦截
  if(to.meta.isLogin == true){
    //判断是否登录过
    if(name == null){
      //若未登录回到登录页面
      next("/");
    }else {
      next();
    }
  }else{
    //不拦截登录
    next();//放行
  }
});
/* 定义权限指令*/
Vue.directive("see",{
  //inserted加载之中执行//el后跟表单元素对象   bingding后跟绑定的值
  inserted:function (el,binding) {
    //获取登录用户的角色信息
    const role = localStorage.getItem("role");
    console.log("role=="+role);
    console.log("binding.value=="+binding.value);
    if(role != binding.value){
      console.log("不可以访问")
      //控制html元素不显示
      el.style.display="none";
    }else{//控制html元素显示
      console.log("可以访问")
      el.style.display="block";
    }
  }
});
//id为指令名字
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})


